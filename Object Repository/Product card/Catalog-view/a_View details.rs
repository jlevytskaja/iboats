<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_View details</name>
   <tag></tag>
   <elementGuidId>78e94a8b-4b9a-4e2b-a3a1-00f9d9e4854c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@href = 'https://qa1.iboats.com/shop/corona-heritage-badger-paint-brush-1-1-2.html' and (text() = 'View details' or . = 'View details')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@id, 'product-list')]//ol/li[2]//span[contains(@class, 'product')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://qa1.iboats.com/shop/corona-heritage-badger-paint-brush-1-1-2.html</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>action view primary</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>View details</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;amasty-shopby-product-list&quot;)/div[@class=&quot;products wrapper grid products-grid&quot;]/ol[@class=&quot;products list items product-items&quot;]/li[@class=&quot;item product product-item&quot;]/div[@class=&quot;product-item-info&quot;]/div[@class=&quot;product details product-item-details&quot;]/div[@class=&quot;product-item-wrapper&quot;]/div[@class=&quot;product-item-column-right&quot;]/div[@class=&quot;product-item-inner&quot;]/div[@class=&quot;product-item-column-right-bottom&quot;]/div[@class=&quot;product actions product-item-actions&quot;]/div[@class=&quot;actions-primary&quot;]/div[1]/a[@class=&quot;action view primary&quot;]</value>
   </webElementProperties>
</WebElementEntity>
