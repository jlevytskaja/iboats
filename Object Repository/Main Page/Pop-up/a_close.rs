<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_close</name>
   <tag></tag>
   <elementGuidId>c8a772bd-b029-401e-8e4c-8e94acb6dc10</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@title = 'Close' and (text() = '×' or . = '×')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;close-button&quot;]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
