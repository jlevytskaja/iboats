<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Home</name>
   <tag></tag>
   <elementGuidId>9a3a54f9-5de0-496d-9e00-fa2008b98c8e</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Go to Home Page</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            Home                        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js&quot;]/body[@class=&quot;page-with-filter page-products categorypath-paint-and-maintenance category-paint-and-maintenance catalog-category-view page-layout-2columns-left radix-dropdown-processed ajax-loading&quot;]/div[@class=&quot;page-wrapper&quot;]/div[@class=&quot;breadcrumbs&quot;]/div[@class=&quot;breadcrumbs-wrapper clearfix&quot;]/ul[@class=&quot;items&quot;]/li[@class=&quot;item home&quot;]/a[1]</value>
   </webElementProperties>
</WebElementEntity>
