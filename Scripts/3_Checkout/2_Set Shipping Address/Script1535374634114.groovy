import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.setText(findTestObject('3_Checkout/1_Checkout Fields/1_Email Address'), 'litaqa@gmail.com')

WebUI.setText(findTestObject('3_Checkout/1_Checkout Fields/2_First Name'), 'Lita')

WebUI.setText(findTestObject('3_Checkout/1_Checkout Fields/3_Last Name'), 'QA')

WebUI.setText(findTestObject('3_Checkout/1_Checkout Fields/4_Street Address'), '123 Test St')

WebUI.setText(findTestObject('3_Checkout/1_Checkout Fields/5_City'), 'Houston')

WebUI.click(findTestObject('3_Checkout/1_Checkout Fields/6_State'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('3_Checkout/1_Checkout Fields/7_State-select'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('3_Checkout/1_Checkout Fields/8_ZIP'), '77055')

WebUI.setText(findTestObject('3_Checkout/1_Checkout Fields/9_Phone Number'), '1234567896')

