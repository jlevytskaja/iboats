<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>8_Verify that user can Continue Shopping after adding product</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-08-27T16:47:13</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>205be04e-e7c4-4b07-9dfe-97d495f76cbe</testSuiteGuid>
   <testCaseLink>
      <guid>41302106-4fb6-44dc-ba20-be528ad72684</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/1_Open_Index</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12744eb4-501b-4b33-b6d5-dcaa46a47ac4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/5_Open PDP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e5ee0f6-fdbe-4c7c-b1e1-b26fc8ac742a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_PDP page/Add to cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5af9367e-70cf-4b68-879a-7543eaa72fa9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/New Test Case</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
