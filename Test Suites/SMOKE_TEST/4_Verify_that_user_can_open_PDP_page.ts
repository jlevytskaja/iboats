<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>4_Verify_that_user_can_open_PDP_page</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-08-27T14:03:56</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>3a3dc660-f345-4adc-ba6e-58e8fd469b10</testSuiteGuid>
   <testCaseLink>
      <guid>6ae9ac42-b8ff-401a-bf84-29115e228477</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/1_Open_Index</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a61068cc-ffab-4704-9622-9054d69ea8fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/3_ Category_Select</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>657bc7e8-7144-45db-9502-f2dcc339025e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/5_Open PDP</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
