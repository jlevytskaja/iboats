<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>5_Verify that user can click on tabs in PDP</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-08-27T15:31:21</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>da7a1fdd-f586-4c6e-8b03-320b52a340d1</testSuiteGuid>
   <testCaseLink>
      <guid>824d0c31-af7f-4989-89e2-10ca5698848e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/1_Open_Index</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>786dd12b-e3b8-41dd-b393-46a32c216d26</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/3_ Category_Select</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>917fd79f-d14d-4265-9135-48995a3493bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/5_Open PDP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>734b1f32-43ae-4edd-be23-e9cba0da580f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_PDP page/PDP-tabs</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
