<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>6_Verify that product can be added to the cart</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-08-27T16:06:21</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>912565ac-3043-49d0-bf26-dae1ba56e315</testSuiteGuid>
   <testCaseLink>
      <guid>8a6e3eff-d7be-41f5-9372-ef9eaaa86e97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/1_Open_Index</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d7ed33ed-e8a2-4bf1-abdd-e61b1bc8f1b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/3_ Category_Select</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7a4534d8-2591-4db2-b300-2648eaec4bbb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/5_Open PDP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fccb6874-5801-4ec2-b6bc-9fe1269a6481</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_PDP page/Add to cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06e7af1a-46e8-4595-a2db-c21535e33c51</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_PDP page/Verify product added to cart</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
