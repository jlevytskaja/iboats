<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>2_Verify_that_user_can_close_banner</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e28b3f9b-64f7-460d-ae5f-2baba18c89e2</testSuiteGuid>
   <testCaseLink>
      <guid>954b7400-6d7b-4312-a3b2-80c3210ce308</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/1_Open_Index</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c136dda-c98a-4370-bff9-f5fb939edd50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/2_Close_Banner</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5b007f4-ad54-4a51-b097-c2aed73c7c6d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/3_ Category_Select</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7c596dd-15dc-4624-a155-81b8d1256eb6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/4_Back_to_Home_Page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
