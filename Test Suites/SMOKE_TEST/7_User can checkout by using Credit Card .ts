<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>7_User can checkout by using Credit Card </name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>863694d6-2a8b-4b5e-8bb5-4453818fbef2</testSuiteGuid>
   <testCaseLink>
      <guid>8b8cd1fc-3f42-43df-abce-cfe5d91b46b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/1_Open_Index</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ec2576eb-6f04-4b2d-ac51-ee2fc6ce8ca5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/3_ Category_Select</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49d7a686-d407-43f2-af97-df6d21288c98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_GIVEN/5_Open PDP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28b75d38-d7c1-432a-87df-cad412d91452</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_PDP page/Add to cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e0631dd-89fa-48da-bf3e-3c73fcf465d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3_Checkout/1_Process to checkout</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8690169c-041b-4c0d-81e3-88f38e5e11a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3_Checkout/2_Set Shipping Address</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b61c414a-9912-4ea1-a6fb-e5ceddcd109c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3_Checkout/4_Set Credit Card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8f65365-0bb4-47f1-9ddc-95e0b8fb78dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3_Checkout/3_Checkout continue</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4af9121f-d37e-4872-8b3f-8c575f91c13f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3_Checkout/5_Place order</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>221b6458-7097-487a-8e29-5346778d402f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3_Checkout/6_Verify order sucess</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
